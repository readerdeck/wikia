package com.readerdeck.wikia.parser

import java.util.regex.Pattern

/**
 * Represents each parsed wikipedia page
 *
 * Note: Code currently adopted from http://code.google.com/p/wikixmlj/source/browse/trunk/src/edu/jhu/nlp/wikipedia/WikiTextParser.java
 *
 * @author Kumar Ishan (@kumarishan)
 */
case class WikiPage(title: String, wikiText: String, id: String) {
  import WikiPage._
  val isRedirect = redirectPattern.matcher(wikiText).find()
  val isStub = stubPattern.matcher(wikiText).find()
  val isDisambiguationPage = disambCatPattern.matcher(wikiText).find()

  private var _links = List.empty[String]
  def links = {
    if(_links.isEmpty){
      val pattern = Pattern.compile("\\[\\[(.*?)\\]\\]", Pattern.MULTILINE)
      val matcher = pattern.matcher(wikiText)
      while(matcher.find){
        Option(matcher.group(1).split("\\|")) match {
          case Some(t) if t.length != 0 =>
            if(t(0).contains(":") == false){
              _links = t(0) :: _links
            }
        }
      }
    }
    _links
  }

  def isSpecialPage =
    if(title.contains("Wikipedia:")||
      title.contains("File:") ||
      title.contains("Template:") ||
      title.contains("Help:") ||
      title.contains("Portal:") ||
      title.contains("List of") ||
      title.contains("MediaWiki:") ||
      title.contains("Category:"))
        true
    else false
}

object WikiPage {
  private val redirectPattern = Pattern.compile("#REDIRECT\\s+\\[\\[(.*?)\\]\\]")
  private val stubPattern = Pattern.compile("\\-stub\\}\\}")
  private val disambCatPattern = Pattern.compile("\\{\\{disambig\\}\\}")
}