package com.readerdeck.wikia.parser

import java.io._
import java.util.regex.Matcher
import scala.util.{Try, Success}
import scala.sys.ShutdownHookThread
import javax.xml.stream.events.XMLEvent._
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLStreamConstants._
import org.apache.commons.lang.StringUtils
import org.codehaus.stax2.XMLInputFactory2
import org.codehaus.stax2.XMLStreamReader2

/**
 * Parser for wikipedia.
 * It assumes input is the standard wikipedia dump provided by Wikipedia.org
 *
 * @author Kumar Ishan
 */
object WikipediaParser {

  class WikiPageBuilder {
    val title = new StringBuilder
    val wikiText = new StringBuilder
    val id = new StringBuilder

    def get = WikiPage(title.toString, wikiText.toString, id.toString)
  }

  /**
   * Different states while parsing wikipedia dump
   */
  sealed trait States {
    def get(implicit xmlr: XMLStreamReader2): PartialFunction[Int, States]
    def default: PartialFunction[Int, States] = {
      case _ => this
    }
  }

  case class Start(process: WikiPage => Unit) extends States {
    def get(implicit xmlr: XMLStreamReader2) = {
      case START_ELEMENT =>
        if(xmlr.getName.toString == "page") InPage(this, process, new WikiPageBuilder)
        else this
    }
  }

  case class InPage(prev: States, process: WikiPage => Unit, builder: WikiPageBuilder) extends States {
    def get(implicit xmlr: XMLStreamReader2) = {
      case START_ELEMENT =>
        if("title" equals xmlr.getName.toString) InTitle(this, builder)
        else if("id" equals xmlr.getName.toString) InId(this, builder)
        else if("text" equals xmlr.getName.toString) InWikiText(this, builder)
        else this
      case CHARACTERS =>
       this
      case END_ELEMENT =>
        if("page" equals xmlr.getName.toString){ process(builder.get); prev }
        else this
    }
  }

  case class InTitle(prev: States, builder: WikiPageBuilder) extends States {
    def get(implicit xmlr: XMLStreamReader2) = {
      case CHARACTERS =>
        builder.title.append(xmlr.getText())
        this
      case END_ELEMENT =>
        prev
    }
  }

  case class InId(prev: States, builder: WikiPageBuilder) extends States {
    def get(implicit xmlr: XMLStreamReader2) = {
      case CHARACTERS =>
        builder.id.append(xmlr.getText())
        this
      case END_ELEMENT =>
        prev
    }
  }

  case class InWikiText(prev: States, builder: WikiPageBuilder) extends States {
    def get(implicit xmlr: XMLStreamReader2) = {
      case CHARACTERS =>
        builder.wikiText.append(xmlr.getText())
        this
      case END_ELEMENT =>
        prev
    }
  }

  /**
   * Usage:
   * WikipediaParser(path, { page: WikiPage =>
   *  ... do something with page extracted ....
   * })
   *
   * @params  path  Path to wikidump file
   * @params  process Callback function that will be called for each new page extracted
   *
   * @author Kumar Ishan (@kumarishan)
   */
  def apply(path: String, process: WikiPage => Unit) = {
    System.setProperty("javax.xml.stream.XMLInputFactory", "com.ctc.wstx.stax.WstxInputFactory")

    (for {
      xmlif <- Try { XMLInputFactory.newInstance.asInstanceOf[XMLInputFactory2] }
      xmlr <- Try {
        xmlif.setProperty(XMLInputFactory.IS_COALESCING, true)
        xmlif.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false)
        xmlif.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false)
        xmlif.setProperty(XMLInputFactory.SUPPORT_DTD, false)
        xmlif.setProperty(XMLInputFactory.IS_VALIDATING, false)

        xmlif.createXMLStreamReader(new FileInputStream(path)).asInstanceOf[XMLStreamReader2]
      }
    } yield xmlr) flatMap { implicit xmlr =>
        var curr: States = Start(process)
        while(xmlr.hasNext){
          curr = curr.get applyOrElse(xmlr.next, {x: Int => curr})
        }
      Success("Done")
    }
  }
}