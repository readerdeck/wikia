import sbt._
import sbt.Classpaths.publishTask
import Keys._
import com.typesafe.sbt.SbtMultiJvm
import com.typesafe.sbt.SbtMultiJvm.MultiJvmKeys.{ MultiJvm, extraOptions, jvmOptions, scalatestOptions, multiNodeExecuteTests, multiNodeJavaName, multiNodeHostsFileName, multiNodeTargetDirName, multiTestOptions }
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys
import sbtassembly.Plugin._
import AssemblyKeys._
import com.typesafe.sbt.SbtStartScript
import com.twitter.scrooge.ScroogeSBT

object WikiaBuild extends Build {

  lazy val root = Project("root", file("."), settings = rootSettings) aggregate(parser)
  lazy val parser = Project("parser", file("parser"), settings = parserSettings)

  def rootSettings = commonSettings ++ Seq(
    publish := {}
  )

  def commonSettings = Defaults.defaultSettings ++ Seq(
    organization := "com.readerdeck",
    version := "1.0.0",
    scalaVersion in ThisBuild := "2.10.2",
    scalacOptions := Seq("-unchecked", "-optimize", "-deprecation", "-feature", "-language:higherKinds", "-language:implicitConversions", "-language:postfixOps", "-Yinline-warnings"),
    retrieveManaged := true,

    fork := true,
    javaOptions += "-Xmx2500M",

    resolvers ++= Seq(
      "Readerdeck Releases" at "http://repo.readerdeck.com/artifactory/readerdeck-releases"
    ),

    publishMavenStyle := true
  ) ++ net.virtualvoid.sbt.graph.Plugin.graphSettings

  def parserSettings = commonSettings ++ Seq(
    name := "wikia-parser",

    publishTo := Some(Resolver.url("readerdeck-releases", new URL("http://repo.readerdeck.com/artifactory/readerdeck-releases"))),

    libraryDependencies ++= Seq(
      "commons-configuration" % "commons-configuration" % "1.9",
      "org.codehaus.woodstox" % "woodstox-core-asl" % "4.2.0",
      "it.unimi.dsi" % "fastutil" % "6.5.11",

      "org.scalaz" %% "scalaz-core" % "7.0.5"
    )
  ) ++ assemblySettings ++ extraAssemblySettings ++ SbtStartScript.startScriptForClassesSettings


  def extraAssemblySettings() = Seq(test in assembly := {}) ++ Seq(
    mergeStrategy in assembly := {
      case m if m.toLowerCase.endsWith("manifest.mf") => MergeStrategy.discard
      case m if m.toLowerCase.matches("meta-inf.*\\.sf$") => MergeStrategy.discard
      case "reference.conf" => MergeStrategy.concat
      case _ => MergeStrategy.first
    }
  )
}